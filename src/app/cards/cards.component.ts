import { Component, OnInit } from '@angular/core';
import { Kitten } from '../_models/kitten';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { AddKittenDialogComponent } from '../add-kitten-dialog/add-kitten-dialog.component';
import { UpdateKittenDialogComponent } from '../update-kitten-dialog/update-kitten-dialog.component';
import { filter } from 'rxjs/operators';
import { KittenService } from '../_services/kitten.service';
import { Item } from '../_models/item';

import {
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ComponentFactory
} from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  public kittens:Kitten [] = [{id: 1 , name :"medor", race:1, color:2, age:'2 juin 1997'}];
  public list:Kitten [] = [];

  breakpoint: number;
  Dialog1Ref: MatDialogRef<AddKittenDialogComponent>;
  Dialog2Ref: MatDialogRef<UpdateKittenDialogComponent>;
  @ViewChild('kittens', { read: ViewContainerRef }) michele: ViewContainerRef;

  constructor(private dialog: MatDialog,private kittenService: KittenService,private resolver: ComponentFactoryResolver) {}

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 1200) ? 1 : 3;
    this.getKittens();
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 1200) ? 1 : 3;
    }
   
  openAddKittenDialog() {
    this.Dialog1Ref = this.dialog.open(AddKittenDialogComponent),
    {
      hasBackdrop: false 
  };
  
  this.Dialog1Ref
      .afterClosed()
      .pipe(filter(newkitten => newkitten))
      .subscribe(newkitten => this.addKitten(newkitten));
       
  }




  openUpdateKittenDialog(kitten: Kitten) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
  
    dialogConfig.data = {
        kitten: kitten,
       
    };
    
    this.Dialog2Ref = this.dialog.open(UpdateKittenDialogComponent , dialogConfig);
    {
      hasBackdrop: false 
  };
  
  this.Dialog2Ref
      .afterClosed()
      .pipe(filter(newkitten => newkitten))
      .subscribe(newkitten => this.updateKitten(newkitten)); 
  }

// --------------------Faux back à l'api---------------
// GET
  getKittens(): void {

  }

// CREATE
  addKitten(kitten: Kitten): void {
    kitten.id = this.kittens.length + 1;
    this.kittens.push(kitten);
  }
// UPDATE
  updateKitten(kitten: Kitten): void {
    this.delete(kitten);
    this.addKitten(kitten);
  }
// DELETE
  delete(kitten: Kitten): void {
    this.kittens = this.kittens.filter(h => h !== kitten);
  }

// --------------------VRAI APPEL à l'api---------------

/*
  // GET
  getKittens(): void {
    this.kittenService.getKittens()
        .subscribe((KITTENS) => this.kittens = KITTENS['kitten']);
  }

// CREATE
  addKitten(kitten: Kitten): void {
    kitten.id = this.kittens.length + 1;
    this.kittenService.addKitten(kitten)
      .subscribe(kitten => { this.kittens.push(kitten)});
  }
// UPDATE
  updateKitten(kitten: Kitten): void {
    this.kittenService.updateKitten(kitten);
  }
// DELETE
  delete(kitten: Kitten): void {
    // this.heroService.deleteKitten(kitten).subscribe();
  }

  */
}





