import { Injectable } from '@angular/core';
import { Kitten } from "../_models/kitten";
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from '../_models/item';
import { catchError, tap } from 'rxjs/operators';
const httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};


@Injectable({
  providedIn: 'root'
})
export class KittenService {


  public kittens:Kitten []= [{id: 1, name: "francis", race: 1, color: 2, age: "2"} ];

  //kittens : Kitten[] = [];

  constructor(  private http: HttpClient,
    private messageService: MessageService) { }

    private KittensUrl = 'http://localhost:3000/api/kittens';  // URL to web api

private log(message: string) {
  this.messageService.add(`HeroService: ${message}`);
  console.log(`KittenService: ${message}`);
}

private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

// GET
 getKittens (): Observable<any> {
  return this.http.get<any>(this.KittensUrl)
    .pipe(
      tap(_ => this.log('fetched kittens')),
      catchError(this.handleError<any>('getKittens', []))
    );
}

// PUT
updateKitten (Kitten: Kitten): Observable<any> {
  return this.http.put(this.KittensUrl, Kitten, httpOptions).pipe(
    tap(_ => this.log(`updated Kitten id=${Kitten.id}`)),
    catchError(this.handleError<any>('updateKitten'))
  );
}

// POST
addKitten (kitten: Kitten): Observable<Kitten[]> {
  console.log(kitten);
  this.kittens.push(kitten);
  console.log(this.kittens);
  return this.http.post<Kitten[]>(this.KittensUrl, this.kittens, httpOptions).pipe(
    tap((newKitten: Kitten[]) => this.log(`added kitten w/ id=${newKitten[0].id}`)),
    catchError(this.handleError<Kitten[]>('addKitten'))
  );
}

// DELETE
deleteHero (kitten: Kitten | number): Observable<Kitten> {
  const id = typeof kitten === 'number' ? kitten : kitten.id;
  const url = `${this.KittensUrl}/${id}`;

  return this.http.delete<Kitten>(url, httpOptions).pipe(
    tap(_ => this.log(`deleted kitten id=${id}`)),
    catchError(this.handleError<Kitten>('deleteKitten'))
  );
} 

}
