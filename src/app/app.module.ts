import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { HomeComponent } from './home/home.component';

import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,MatNativeDateModule, MatListModule, MatInputModule, MatFormFieldModule, MatRippleModule,MatSelectModule,MatDatepickerModule } from '@angular/material';
import { MainNavComponent } from './main-nav/main-nav.component';
import { FormsComponent } from './forms/forms.component'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <-- NgModel lives here
import {MatDialogModule} from '@angular/material/dialog';
import { AddKittenDialogComponent } from './add-kitten-dialog/add-kitten-dialog.component';
import { CardsComponent } from './cards/cards.component';
import {MatCardModule} from '@angular/material/card'; 
import {MatGridListModule} from '@angular/material/grid-list';
import { LoginComponent } from './login/login.component'; 
import { HttpClientModule }    from '@angular/common/http';
import { UpdateKittenDialogComponent } from './update-kitten-dialog/update-kitten-dialog.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: '',
  redirectTo: '/login',
  pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainNavComponent,
    FormsComponent,
    AddKittenDialogComponent,
    CardsComponent,
    LoginComponent,
    UpdateKittenDialogComponent
    
  ],

  imports: [
    BrowserModule,FormsModule,MatDialogModule,ReactiveFormsModule,   MatButtonModule,
    MatFormFieldModule,MatCardModule,MatGridListModule, HttpClientModule,MatSelectModule,
    MatInputModule,MatDatepickerModule,MatNativeDateModule,
    MatRippleModule,
    AppRoutingModule,BrowserAnimationsModule, RouterModule.forRoot(appRoutes), LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule
  ],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AddKittenDialogComponent,UpdateKittenDialogComponent]
})
export class AppModule { }


