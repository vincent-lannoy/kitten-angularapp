import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateKittenDialogComponent } from './update-kitten-dialog.component';

describe('UpdateKittenDialogComponent', () => {
  let component: UpdateKittenDialogComponent;
  let fixture: ComponentFixture<UpdateKittenDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateKittenDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateKittenDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
