import { Component, OnInit, Inject } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from '../app.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Kitten } from '../_models/kitten';

@Component({
  selector: 'app-update-kitten-dialog',
  templateUrl: './update-kitten-dialog.component.html',
  styleUrls: ['./update-kitten-dialog.component.css']
})
export class UpdateKittenDialogComponent implements OnInit {

  newkitten:Kitten = {id:1 ,name:"", race:1, age:'', color:1};
  
  form: FormGroup;

  constructor(    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdateKittenDialogComponent>,@Inject(MAT_DIALOG_DATA) data) 
    { this.newkitten = data.kitten}

  
  ngOnInit() {
    console.log("---------------------------");

    //this.newkitten = this.data;
  }

  submit() {

    console.log(this.newkitten);

    // this.newkitten.name = form.value.name;
    this.dialogRef.close(this.newkitten);}

}
