export class Kitten {
    public id: number;
    public name: string;
    public color: number; //0-blanc 1-roux 2-noir
    public race: number;//0-siamois 2-sphynx 3-européen
    public age: string;
}