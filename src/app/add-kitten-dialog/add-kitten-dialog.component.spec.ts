import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddKittenDialogComponent } from './add-kitten-dialog.component';

describe('AddKittenDialogComponent', () => {
  let component: AddKittenDialogComponent;
  let fixture: ComponentFixture<AddKittenDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddKittenDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddKittenDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
