import { Component, OnInit } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from '../app.module';

import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Kitten } from '../_models/kitten';



@Component({
  selector: 'app-add-kitten-dialog',
  templateUrl: './add-kitten-dialog.component.html',
  styleUrls: ['./add-kitten-dialog.component.css']
})
export class AddKittenDialogComponent implements OnInit {

  newkitten:Kitten = {id:1 ,name:"", race:1, age:'', color:1};
  
  form: FormGroup;

  
  constructor(    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddKittenDialogComponent>) { }

  ngOnInit() {
    // this.form = this.formBuilder.group({
    //   name: ''})
  }

  
  submit() {

    console.log(this.newkitten);

    // this.newkitten.name = form.value.name;
    this.dialogRef.close(this.newkitten);}
  
}


