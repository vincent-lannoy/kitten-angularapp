import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AddKittenDialogComponent } from '../add-kitten-dialog/add-kitten-dialog.component';
import { filter } from 'rxjs/operators';
import { Kitten } from '../_models/kitten';
import { KittenService } from '../_services/kitten.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public kittens:Kitten [];

  DialogRef: MatDialogRef<AddKittenDialogComponent>;

  constructor(private dialog: MatDialog,private kittenService: KittenService) { }

  ngOnInit() {
    // this.getHeroes();
  }

  // getHeroes(): void {
  //   this.kittenService.getHeroes()
  //       .subscribe(KITTENS => this.kittens = KITTENS);
  // }

  // callUrl():any {
  //   this.kittenService.getKittens2();
  // }

  // openAddKittenDialog() {
  //   this.DialogRef = this.dialog.open(AddKittenDialogComponent),
  //   {
  //     hasBackdrop: false 
  // };
  
  // this.DialogRef
  //     .afterClosed()
  //     .pipe(filter(name => name))
  //     .subscribe(name => this.kittens.push({ name, id: this.kittens.length })); 
  // }
  

  // delete(kitten: Kitten): void {
  //   this.kittens = this.kittens.filter(h => h !== kitten);
  //   // this.heroService.deleteHero(hero).subscribe(); POUR LE SERVEUR
  // }
}
